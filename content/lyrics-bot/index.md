---
title: Lyrics bot
---

look at [dis](https://codeberg.org/snvmk/lyrics-bot)

a Telegram bot for searching lyrics with Inline Query support.
uses [genius-lyrics](https://npm.im/genius-lyrics) and [telegraf](https://telegraf.js.org).
pretty easy to start yourself. have you some VM from [heyderdahl](https://heyderdahl.com)
(you no scam anymore sry Joe), clone bot repo to it,

```sh
$ # BOT -> botfather
$ # GENIUS -> https://genius.com/api-clients/new > Generate Access Token
$ echo -e 'BOT_TOKEN="_"\nGENIUS_TOKEN="_' > .env
$ npm i
installed some packages, and audited same
$ node .
```

and now you send it some search term, or the audio itself, or just
inline in any chat - and it gives you lyrics. the inline method is
most convenciable, because you can choose which track you need lyrics of.
