---
title: who is neovim
date: 2022-07-29T22:11:33+0700
---

you ask, who is neovim, and why (sometimes) im talking about it on discord as the best editor ever? like, why it will not be the best one when it:
- starts up in 0 seconds
- have the all power of other more heavy editors like VSCode, JetBrains things, Atom (R.I.P) ...
- and just looks nice

also, from same company, I use:
- sway, the i3 (a window manager) of wayland world
- aerc, the email client for terminal emulator
- cmus, the music player for terminal emulator

