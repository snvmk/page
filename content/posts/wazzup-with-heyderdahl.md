---
title: "wazzup with heyderdahl"
date: 2022-07-26T15:10:00+07:00
---

something is wrong with [heyderdahl panel](https://panel.heyderdahl.me). it literally removed my ssl certificate, for both servers (yes, i have two of them). only site is available. matrix chat and gitea repos are offline (or at least, non-ssl, i will look into CF dashboard to disable https-only). thanks
