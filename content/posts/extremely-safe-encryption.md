---
title: Extremely safe encryption!
date: 2022-08-19T18:32:00+07:00
---

i have a friend from FBI. recently, he gave me a riddle to decrypt a string. he gave me few hints, but i didnt guess even with them.

so, the method is simple but strong: encode it with base64 EIGHT TIMES (or more, if you dont mind length). no one will guess it.

thats all for today :)
